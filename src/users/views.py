from django.shortcuts import render
from django.contrib.auth.forms import UserCreationForm
from django.contrib import messages
from .forms import UserRegisterForm
from django.http import HttpResponseRedirect


def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'{username},Your account has been created successfully! you are now able to Log in.')
            return HttpResponseRedirect('/')
    else:
        form = UserRegisterForm()
    return render(request, 'users/register.html', {'form': form})

    
